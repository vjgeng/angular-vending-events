import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-employee-register',
  templateUrl: './employee-register.component.html',
  styleUrls: ['./employee-register.component.css']
})
export class EmployeeRegisterComponent implements OnInit {

  public employee: { message : String };
  public errorMsg: string;

  constructor(private _employeeService: EmployeeService){}

   ngOnInit(){
        this._employeeService.getEmployees()
            .subscribe( resEmployeeData => this.employee = resEmployeeData,
                        resEmployeeError => this.errorMsg = resEmployeeError);
        
    }



}